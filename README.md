# Diffusion Diagnostic Reporter Examples

Diffusion provides diagnostic information about the server as Mbeans which can 
be accessed directly via JMX, or as topics which are exposed by the 
[JMX Adapter](https://docs.pushtechnology.com/docs/5.9.9/manual/html/administratorguide/systemmanagement/t_using-jmx.html).

This repository contains two example projects which use different approaches 
for obtaining this information:

* Java JMX API - Connects directly to the server via JMX
* Node.js Client - Connects to the server as a Diffusion client and subscribes 
to MBean topics