/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.pushtechnology.diffusion.examples;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Connects to JMX connector and records MBeans related to JVM memory use.
 *
 * @author Push Technology Limited
 */
//CHECKSTYLE.OFF: UncommentedMain
public final class JmxReporter {
    private static final Logger LOG = LoggerFactory.getLogger(JmxReporter.class);
    /**
     * Rate at which to poll MBeans in seconds.
     */
    private static final int UPDATE_RATE = 10;

    private final JMXConnector connector;
    private final MBeanServerConnection connection;

    private final Statistics stats;

    private final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

    private final CSVLogger logger;

    private final ObjectName serverMbean;
    private final ObjectName clientMbean;

    /**
     * Application entry point.
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        final CmdOptions options = new CmdOptions();
        final CmdLineParser parser = new CmdLineParser(options);
        final Statistics stats = new Statistics();

        try {
            parser.parseArgument(args);

            final JmxReporter jmxReporter = JmxReporterFactory.create(options.getJmxUrl(), options.getJmxPrincipal(),
                    options.getJmxCreds(), stats);
            jmxReporter.gatherAttributes();

            LOG.info("Press enter to quit");
            System.in.read();
            jmxReporter.close();
        }
        catch (MalformedObjectNameException e) {
            LOG.error("Could not find expected MBean Object", e);
            e.printStackTrace();
            System.exit(1);
        }
        catch (CmdLineException e) {
            LOG.error("There was a problem with the supplied arguments");
            e.printStackTrace();
            System.exit(1);
        }
        catch (IOException e) {
            LOG.error("There was a problem writing to the CSV");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Constructor.
     */
    public JmxReporter(JMXConnector connector, MBeanServerConnection connection, ObjectName serverMbean,
                       ObjectName clientMbean, Statistics stats, CSVLogger logger) {
        this.connector = connector;
        this.connection = connection;
        this.serverMbean = serverMbean;
        this.clientMbean = clientMbean;
        this.stats = stats;
        this.logger = logger;
    }

    /**
     * Collects MBeans from the server.
     */
    public void gatherAttributes() throws IOException {
        monitorAttribute(serverMbean, "NumberOfTopics", "Topic Count");
        monitorAttribute(serverMbean, "UsedPhysicalMemorySize", "Used Physical Memory");
        monitorAttribute(clientMbean, "ConcurrentClientCount", "Clients Count");

        logger.writeRow(stats.getColumns());

        executorService.scheduleAtFixedRate(() -> {
            try {
                stats.getMbeans().forEach(mbean -> updateAttribute(mbean.getKey(), mbean.getValue()));
                logger.writeRow(stats.getLatest());
            }
            catch (IOException e) {
                LOG.error("There was a problem writing to the CSV");
                e.printStackTrace();
                System.exit(1);
            }
        }, UPDATE_RATE, UPDATE_RATE, TimeUnit.SECONDS);
    }

    /**
     * Stops logging, closes the JMX connection and closes the log file.
     *
     * @throws IOException if the JMX connection cannot be closed cleanly
     */
    public void close() throws IOException, InterruptedException {
        executorService.shutdown();
        executorService.awaitTermination(30, TimeUnit.SECONDS);
        connector.close();
        try {
            logger.close();
        }
        catch (IOException e) {
            LOG.error("There was an error closing the CSV log file");
            e.printStackTrace();
        }
        finally {
            LOG.warn("JMX connection closed");
        }
    }

    /**
     * Updates statistic with latest value of MBean attribute.
     *
     * @param oName Object name
     * @param attrName Attribute name
     */
    private void updateAttribute(ObjectName oName, String attrName) {
        try {
            final Object attrValue = connection.getAttribute(oName, attrName);
            stats.update(oName, attrName, attrValue.toString());
        }
        catch (IOException | ReflectionException | InstanceNotFoundException | AttributeNotFoundException |
                MBeanException e) {
            LOG.error("There was a problem getting JMX attribute");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void monitorAttribute(ObjectName objectName, String attrName, String column) {
        stats.set(objectName, attrName, column);
    }
}
