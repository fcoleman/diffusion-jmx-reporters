/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.pushtechnology.diffusion.examples;

import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Utility class that creates and opens a log file and writes to it in CSV format.
 *
 * @author Push Technology Limited
 */
public final class CSVLogger {

    private final FileWriter fileWriter;
    private final CSVPrinter csvPrinter;

    /**
     * Constructor.
     */
    public CSVLogger(FileWriter fileWriter, CSVPrinter csvPrinter) {
        this.fileWriter = fileWriter;
        this.csvPrinter = csvPrinter;
    }

    /**
     * Write a row to the CSV.
     */
    public void writeRow(List<String> row) throws IOException {
        csvPrinter.printRecord(row);
        fileWriter.flush();
    }

    /**
     * Close the CSV.
     */
    public void close() throws IOException {
        csvPrinter.close(); // this also closes the FileWriter
    }

}
