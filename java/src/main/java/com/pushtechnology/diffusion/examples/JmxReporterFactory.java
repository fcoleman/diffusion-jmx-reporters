/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.pushtechnology.diffusion.examples;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * Creates a JMXReporter.
 *
 * @author Push Technology Limited
 */
public final class JmxReporterFactory {
    private static final Logger LOG = LoggerFactory.getLogger(JmxReporterFactory.class);
    private static final Pattern PATTERN = Pattern.compile("^(.+?):(\\d+)$");

    private JmxReporterFactory() { }

    /**
     * Connects to JMX and determines the server name.
     *
     * @throws NullPointerException if jmxUrl, principal, or credentials are null
     * @throws IOException if the connection attempt failed
     * @return JmxReporter an instance of JmxReporter
     */
    public static JmxReporter create(String jmxUrl, String principal, String credentials, Statistics stats)
            throws IOException, MalformedObjectNameException {
        requireNonNull(jmxUrl, "JMX URL is required");
        requireNonNull(principal, "Principal is required");
        requireNonNull(credentials, "Credentials are required");

        final JMXConnector connector = JMXConnectorFactory.connect(
                new JMXServiceURL(expandToURL(jmxUrl)),
                Collections.singletonMap(JMXConnector.CREDENTIALS, new String[] {
                        principal,
                        credentials
                })
        );

        final MBeanServerConnection connection = connector.getMBeanServerConnection();
        final String serverObject = "com.pushtechnology.diffusion:type=Server,server=\"*\"";
        final Set<ObjectName> nameSet = connection.queryNames(
                new ObjectName(serverObject), null);

        if (nameSet.size() < 1) {
            LOG.error("Cannot find any MBeans matching {}", serverObject);
            System.exit(1);
        }

        final String server = nameSet.iterator().next().getKeyProperty("server");

        if (server == null) {
            LOG.error("Server property not found");
            System.exit(1);
        }

        final String mbeansRoot = "com.pushtechnology.diffusion:server=" + server;
        final ObjectName serverMbean = new ObjectName(mbeansRoot + ",type=Server");
        final ObjectName clientMbean = new ObjectName(mbeansRoot + ",type=ClientStatistics");

        final Date now = new Date();
        final String fileName = "log-" + String.valueOf(now.getTime()) + ".log";

        final FileWriter fileWriter = new FileWriter(fileName);
        final CSVPrinter csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT.withRecordSeparator("\n"));

        final CSVLogger logger = new CSVLogger(fileWriter, csvPrinter);

        return new JmxReporter(connector, connection, serverMbean, clientMbean, stats, logger);
    }

    private static String expandToURL(String url) {
        final Matcher matcher = PATTERN.matcher(url);
        return matcher.matches() ?
                String.format("service:jmx:rmi:///jndi/rmi://%s:%s/jmxrmi", matcher.group(1), matcher.group(2)) :
                url;
    }
}