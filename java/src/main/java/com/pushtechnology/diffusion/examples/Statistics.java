/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.pushtechnology.diffusion.examples;

import javafx.util.Pair;

import javax.management.ObjectName;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains data about MBeans that is logged.
 *
 * @author Push Technology Limited
 */
public class Statistics {
    /**
     * Constant used for timestamp column.
     */
    private static final String TIMESTAMP = "Timestamp";

    /**
     * A list of columns that relate to the MBeans being monitored.
     */
    private final List<String> columns = new ArrayList<>();

    /**
     * Mapping of MBean to column name.
     */
    private final Map<Pair<ObjectName, String>, String> mbeanToColumn = new LinkedHashMap<>();

    /**
     * Mapping of column name to value.
     */
    private final Map<String, String> stats = new HashMap<>();

    Statistics() {
        columns.add(TIMESTAMP);
    }

    /**
     * Sets a MBean that will be monitored.
     */
    public void set(ObjectName mbeanObj, String mbeanAttr, String column) {
        mbeanToColumn.put(new Pair<>(mbeanObj, mbeanAttr), column);
        columns.add(column);
    }

    /**
     * Updates the value of an MBean.
     */
    public void update(ObjectName mbeanObj, String mbeanAttr, String value) {
        final String column = mbeanToColumn.get(new Pair<>(mbeanObj, mbeanAttr));
        if (column == null) {
            throw new NullPointerException("Unknown..");
        }
        stats.put(column, value);
    }

    /**
     * @return list containing columns
     */
    public List<String> getColumns() {
        return columns;
    }

    /**
     * @return latest values for MBeans, the first record is a timestamp
     */
    public List<Pair<ObjectName, String>> getMbeans() {
        final List<Pair<ObjectName, String>> mbeans = new ArrayList<>();
        mbeanToColumn.forEach((mbean, s) -> mbeans.add(mbean));
        return mbeans;
    }

    /**
     * @return list containing the latest values, this is output in column order
     */
    public List<String> getLatest() {
        final Date now = new Date();
        final List<String> latest = new ArrayList<>();
        stats.put(TIMESTAMP, String.valueOf(now.getTime()));
        columns.forEach(column -> latest.add(stats.get(column)));
        return latest;
    }
}
