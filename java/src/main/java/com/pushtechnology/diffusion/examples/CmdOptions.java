/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.pushtechnology.diffusion.examples;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

/**
 * Command line options.
 *
 * @author Push Technology Limited
 */
public class CmdOptions {

    @Argument(index = 0, required = true, metaVar = "JMX-URL",
            usage = "JMX URL, e.g. localhost:1099 or service:jmx:rmi://localhost:1100/jndi/rmi://localhost:1099/jmxrmi")
    private String jmxUrl;

    @Option(name = "-principal", usage = "Optional JMX principal (aka username)")
    private String jmxPrincipal;


    @Option(name = "-creds", usage = "Optional credentials (or password) for JMX connection")
    private String jmxCreds;

    public String getJmxUrl() {
        return jmxUrl;
    }

    public void setJmxUrl(String jmxUrl) {
        this.jmxUrl = jmxUrl;
    }

    public String getJmxCreds() {
        return jmxCreds;
    }

    public void setJmxCreds(String jmxCreds) {
        this.jmxCreds = jmxCreds;
    }

    public String getJmxPrincipal() {
        return jmxPrincipal;
    }

    public void setJmxPrincipal(String jmxPrincipal) {
        this.jmxPrincipal = jmxPrincipal;
    }
}
