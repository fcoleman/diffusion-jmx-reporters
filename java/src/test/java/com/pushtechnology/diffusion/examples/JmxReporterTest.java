package com.pushtechnology.diffusion.examples;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;

import java.io.FileWriter;
import java.io.IOException;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class JmxReporterTest {

    @Mock
    JMXConnector connector;

    @Mock
    MBeanServerConnection serverConnection;

    @Mock
    ObjectName serverMbean;

    @Mock
    ObjectName clientMbean;

    @Mock
    Statistics statistics;

    @Mock
    FileWriter fileWriter;

    CSVPrinter csvPrinter;

    CSVLogger csvLogger;

    private JmxReporter jmxReporter;

    @Before
    public void setUp() throws IOException {
        initMocks(this);
        csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT.withRecordSeparator("\n"));
        csvLogger = new CSVLogger(fileWriter, csvPrinter);
        jmxReporter = new JmxReporter(connector, serverConnection, serverMbean, clientMbean, statistics, csvLogger);
    }

    @Test
    public void closes() throws IOException, InterruptedException {
        jmxReporter.close();

        verify(connector).close();
        verify(fileWriter).close();
    }

    @Test
    public void gathersAttributes() throws IOException {
        jmxReporter.gatherAttributes();

        verify(statistics, times(3)).set(isA(ObjectName.class), isA(String.class), isA(String.class));
        verify(fileWriter).flush();
    }
}
