package com.pushtechnology.diffusion.examples;

import javafx.util.Pair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.management.ObjectName;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

public class StatisticsTest {

    private Statistics statistics;

    @Mock
    private ObjectName objectName;

    @Before
    public void setUp() {
        initMocks(this);
        statistics = new Statistics();
    }

    @Test
    public void containsTimestampColumn() throws Exception {
        assertEquals("Timestamp", statistics.getColumns().get(0));
    }

    @Test
    public void setsMbean() {
        statistics.set(objectName, "attr", "col");
        assertEquals("col", statistics.getColumns().get(1));
    }

    @Test
    public void getsMbean() {
        statistics.set(objectName, "attr", "col");
        assertEquals(new Pair(objectName, "attr"), statistics.getMbeans().get(0));
    }

    @Test
    public void updatesMbean() {
        final String mbeanAttr = "attribute";
        final String column = "col";
        statistics.set(objectName, mbeanAttr, column);
        statistics.update(objectName, mbeanAttr, "FOO");
        assertEquals("FOO", statistics.getLatest().get(1));
    }

}
