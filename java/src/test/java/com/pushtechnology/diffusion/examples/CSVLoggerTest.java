package com.pushtechnology.diffusion.examples;

import com.pushtechnology.diffusion.examples.CSVLogger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class CSVLoggerTest {

    @Mock
    private FileWriter fileWriter;

    private CSVPrinter csvPrinter;

    private CSVLogger csvLogger;

    @Before
    public void setUp() throws IOException {
        initMocks(this);
        csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT.withRecordSeparator("\n"));
        csvLogger = new CSVLogger(fileWriter, csvPrinter);

    }

    @Test
    public void canWriteRow() throws IOException {
        final String[] columns = { "foo", "bar", "baz" };
        final List<String> row = Arrays.asList(columns);

        csvLogger.writeRow(row);
        verify(fileWriter).flush();
    }

    @Test
    public void canClose() throws IOException {
        csvLogger.close();

        verify(fileWriter).close();
    }

}
