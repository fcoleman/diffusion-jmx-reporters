# Java Diffusion Diagnostics Reporter

An example demonstrating how to connect to a Diffusion server with the JMX API 
and collect diagnostic information.

The data collected is logged to a CSV file.

## Usage

First you will need to compile with:

```
mvn clean install
```
Then you can run the application as follows:

```
java -jar target/jmx-recorder-1.0-SNAPSHOT.jar localhost:1099 -principal admin -creds password
```
The arguments are: JMX-URL principal credentials

## Authentication
JMX connections are authenticated using the same process as Diffusion 
connections. JMX connections will be evaluated by the authentication handlers 
defined in the security section of the Server.xml
