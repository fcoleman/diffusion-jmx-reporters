/*******************************************************************************
 * Copyright (C) 2017 Push Technology Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

const diffusion = require('diffusion');
const fs = require('fs');

if (process.argv.length < 6) {
	console.log("Missing required arguments; host port principal credentials");
	console.log("Example: node run.js localhost 8080 admin password");
	process.exit(1);
}

const options = {
	host: process.argv[2],
	port: process.argv[3],
	principal: process.argv[4],
	credentials: process.argv[5]
};

const LOG_FILE = "mbeans-" + Date.now() + ".log";
const SERVER_TOPIC = "Diffusion";
const TIMEOUT = 1000 * 60;

let session;

function Stats() {
	// Columns in order to be output to log file
	this.columns = ['Timestamp'];

	// Map of column to latest value
	this.stats = {};

	// Translates a topic path to a column name
	this.topicToColumn = {};

	/**
	 * Sets a mapping of topic path to column name.
	 */
	this.set = function(topic, column) {
		this.topicToColumn[topic] = column;
		this.stats[column] = null;
		this.columns.push(column)
	}

	/**
	 * Updates a recorded statistic.
	 * Throws an error if the topic path has not been mapped to a column name.
	 */
	this.update = function(topic, value) {
		const column = this.topicToColumn[topic];
		if (!column) {
			throw new Error('Update received for unexpected topic: ' + topic);
		}
		this.stats[column] = value;
	}

	/**
	 * Returns an array containing the latest stats.
	 */
	this.getLatest = function() {
		let latest = [];
		for (let i = 0; i < this.columns.length; i++) {
			latest.push(this.stats[this.columns[i]]);
		}
		return latest;
	}
}

const stats = new Stats();

/**
 * Asyncronously fetches the server name and resolves a promise
 * containing this value.
 */
function getServerName() {
	console.log("Connected to diffusion");

	return new Promise(function(fulfill, reject) {
		session.fetch(SERVER_TOPIC).on({
			value: function(state, path) {
				const meta = new diffusion.metadata.RecordContent();
				meta.addRecord('record', { min: 1, max: -1 })
					.addField('field', meta.string(), { min: 1, max: -1 });

				const serverName = meta.parse(state)
					.records()[0]
					.fields()[1];

				fulfill(serverName);
			},
			error: function(err) {
				reject(err);
			}
		});
	});
}

function subscribeToMbeans(serverName) {
	const topicRoot = SERVER_TOPIC + "/MBeans/com/pushtechnology/diffusion/server=\"" + serverName + "\"";
	const serverMbeans = topicRoot + "/type=Server/Attributes/";
	const clientStatisticsMbeans = topicRoot + "/type=ClientStatistics/Attributes/";

	const topicsTopic = serverMbeans + "NumberOfTopics";
	const clientsTopic = clientStatisticsMbeans + "ConcurrentClientCount";
	const memoryTopic = serverMbeans + "UsedPhysicalMemorySize"

	function recordMbean(topic, columnName) {
		stats.set(topic, columnName);
		session.subscribe(topic)
			.on({
				update: function(update, topic) {
					stats.update(topic, parseInt(update));
				},
				error: function(error) {
					console.log('Error subscribing to MBeans topic: ' + error);
					process.exit(1);
				}
			});
	}

	recordMbean(topicsTopic, "Topic Count");
	recordMbean(clientsTopic, "Concurrent Clients");
	recordMbean(memoryTopic, "Used Physical Memory");

	writeLogLine(stats.columns.join(','));
}

function writeLogLine(line) {
	fs.appendFile(LOG_FILE, line + "\n", function(err) {
		if (err) {
			console.log('Error writing to log');
			process.exit(1);
		}
		console.log('Logged line...');
	});
}

diffusion.connect(options)
	.then(function(s) {
		session = s;
		session.on({
			disconnect: function() {
				console.log('Disconnected from diffusion');
			},
			reconnect: function() {
				console.log('Reconnected to diffusion');
			},
			error: function(error) {
				console.log('Session error: ' + error);
			},
			close: function() {
				console.log('Session closed');
			}
		});
		return getServerName();
	}, function(err) {
		console.log('Error connecting to diffusion: ' + err);
		process.exit(1);
	})
	.then(subscribeToMbeans, function(err) {
		console.log('Error fetching server name: ' + err);
		process.exit(1);
	})
	.then(function() {
		console.log("Logging MBeans to file every " + TIMEOUT / 1000 + " seconds");
		setInterval(function() {
			writeLogLine(Date.now() + stats.getLatest().join(','));
		}, TIMEOUT);
	});