# Node.js Diffusion Diagnostics Reporter

Diffusion has a JMX Adapter which, when enabled, exposes diagnostic information 
about the server such as topic count, client count, CPU usage, and JVM specific 
diagnostics such as heap memory usage.

This is an example Node.js client which connects to a Diffusion server and 
subscribes to some of these topics, logging the data to a CSV file for 
processing.

## Usage

First you will need to install the dependencies with:

```
npm install
```
Then you can run the application as follows:

```
node run.js localhost 8080 admin password 
```
The arguments are: host port principal credentials

## Permission
For this to work the principal used to connect needs to be assigned a role 
with 
[topic-scoped permission](https://docs.pushtechnology.com/docs/5.9.9/manual/html/designguide/security/permissions_reference.html) 
for the "Diffusion" topic.
